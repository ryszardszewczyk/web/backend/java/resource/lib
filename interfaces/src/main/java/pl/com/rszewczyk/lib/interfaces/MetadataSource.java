package pl.com.rszewczyk.lib.interfaces;

import java.io.IOException;
import java.io.InputStream;
import javax.validation.constraints.NotNull;

public interface MetadataSource {
  @NotNull
  InputStream getInputStream() throws IOException;

  @NotNull
  String getContentType();

  @NotNull
  String getName();
}
