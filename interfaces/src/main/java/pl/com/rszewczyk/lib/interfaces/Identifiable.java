package pl.com.rszewczyk.lib.interfaces;

public interface Identifiable<T> {
  T getId();
}
