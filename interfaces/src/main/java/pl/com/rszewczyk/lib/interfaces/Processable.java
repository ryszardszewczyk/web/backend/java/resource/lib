package pl.com.rszewczyk.lib.interfaces;

import javax.validation.constraints.NotNull;

public interface Processable {
  boolean canProcess(@NotNull Class<?> clazz);
}
