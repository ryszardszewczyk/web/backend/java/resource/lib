package pl.com.rszewczyk.structures;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.com.rszewczyk.lib.interfaces.MetadataSource;

@RequiredArgsConstructor
public class ByteArrayMetadataSource implements MetadataSource {
  @Getter private final String name;
  private final byte[] bytes;
  @Getter private final String contentType;

  @Override
  public InputStream getInputStream() {
    return new ByteArrayInputStream(this.bytes);
  }

  @Override
  public String toString() {
    return "ByteArrayMetadataSource{name='"
        + this.name
        + "', contentType='"
        + this.contentType
        + "'}";
  }
}
