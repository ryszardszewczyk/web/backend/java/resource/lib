package pl.com.rszewczyk.structures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import javax.validation.constraints.NotNull;

public interface Triple<A, B, C> extends Pair<A, B> {
  @NotNull
  C third();

  static <A, B, C> Triple<A, B, C> of(@NotNull A first, @NotNull B second, @NotNull C third) {
    return new Tuple3(first, second, third);
  }

  static <A, B, C> Triple<List<A>, List<B>, List<C>> partition(
      @NotNull Collection<Triple<A, B, C>> triples) {
    List<A> firsts = new ArrayList<>(triples.size());
    List<B> seconds = new ArrayList<>(triples.size());
    List<C> thirds = new ArrayList<>(triples.size());

    triples.forEach(
        triple -> {
          firsts.add(triple.first());
          seconds.add(triple.second());
          thirds.add(triple.third());
        });

    return of(firsts, seconds, thirds);
  }

  default <R1, R2, R3> Triple<R1, R2, R3> map(
      @NotNull Function<A, R1> firstMapper,
      @NotNull Function<B, R2> secondMapper,
      @NotNull Function<C, R3> thirdMapper) {
    return of(
        firstMapper.apply(this.first()),
        secondMapper.apply(this.second()),
        thirdMapper.apply(this.third()));
  }
}
