package pl.com.rszewczyk.structures;

import java.util.List;
import java.util.function.Supplier;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Tuple3<A, B, C> implements Triple<A, B, C> {
  private final A first;
  private final B second;
  private final C third;

  @Override
  public A first() {
    return this.first;
  }

  @Override
  public B second() {
    return this.second;
  }

  @Override
  public C third() {
    return this.third;
  }

  @Override
  public List<Supplier<Object>> methods() {
    return List.of(this::first, this::second, this::third);
  }

  @Override
  public String toString() {
    return String.format("(%s, %s, %s)", this.first, this.second, this.third);
  }
}
