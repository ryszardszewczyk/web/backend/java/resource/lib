package pl.com.rszewczyk.structures;

import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import lombok.Getter;

@Getter
public class ApiResponse<T> {
  private final int statusCode;
  private final Map<String, List<String>> headers;
  private final T data;

  public ApiResponse(int statusCode, @NotNull Map<String, List<String>> headers) {
    this(statusCode, headers, null);
  }

  public ApiResponse(int statusCode, @NotNull Map<String, List<String>> headers, @Nullable T data) {
    this.statusCode = statusCode;
    this.headers = headers;
    this.data = data;
  }
}
