package pl.com.rszewczyk.structures;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum TriState {
  YES,
  NO,
  UNDETERMINED;
}
