package pl.com.rszewczyk.structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public interface Either<L, R> {
  <X> X reduce(
      @NotNull Function<? super L, ? extends X> left,
      @NotNull Function<? super R, ? extends X> right);

  void Consume(@NotNull Consumer<? super L> left, @NotNull Consumer<? super R> right);

  <L2, R2> @NotNull Either<L2, R2> map(
      @NotNull Function<L, L2> left, @NotNull Function<R, R2> right);

  boolean isLeft();

  boolean isRight();

  L fromLeft(@Nullable L defaultValue);

  R fromRight(@Nullable R defaultValue);

  L leftOr(@NotNull Function<? super R, ? extends L> mapper);

  R rightOr(@NotNull Function<? super L, ? extends R> mapper);

  static <L, R> Either<L, R> left(@NotNull L left) {
    return new EitherLeft(left);
  }

  static <L, R> Either<L, R> right(@NotNull R right) {
    return new EitherRight(right);
  }

  static <L, R> List<L> lefts(@Nullable List<? extends Either<L, R>> eithers) {
    return Objects.nonNull(eithers) && !eithers.isEmpty()
        ? eithers.stream().filter(Either::isLeft).map(i -> i.fromLeft(null)).toList()
        : Collections.emptyList();
  }

  static <L, R> List<R> rights(@Nullable List<? extends Either<L, R>> eithers) {
    return Objects.nonNull(eithers) && !eithers.isEmpty()
        ? eithers.stream().filter(Either::isRight).map(i -> i.fromRight(null)).toList()
        : Collections.emptyList();
  }

  static <L, R> Pair<List<L>, List<R>> partition(@Nullable List<? extends Either<L, R>> eithers) {
    if (Objects.nonNull(eithers) && !eithers.isEmpty()) {
      int size = eithers.size();
      int leftSize = (int) eithers.stream().filter(Either::isLeft).count();
      List<L> lefts = new ArrayList<>(leftSize);
      List<R> rights = new ArrayList<>(size - leftSize);

      eithers.forEach(
          (Either<L, R> either) -> {
            if (either.isLeft()) {
              lefts.add(either.fromLeft(null));
            } else {
              rights.add(either.fromRight(null));
            }
          });

      return Pair.of(lefts, rights);
    } else {
      return Pair.of(Collections.emptyList(), Collections.emptyList());
    }
  }
}
