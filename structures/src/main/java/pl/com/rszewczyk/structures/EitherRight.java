package pl.com.rszewczyk.structures;

import java.util.function.Consumer;
import java.util.function.Function;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EitherRight<L, R> implements Either<L, R> {
  private final R value;

  @Override
  public <X> X reduce(
      Function<? super L, ? extends X> left, Function<? super R, ? extends X> right) {
    return right.apply(value);
  }

  @Override
  public void Consume(Consumer<? super L> left, Consumer<? super R> right) {
    right.accept(value);
  }

  @Override
  public @NotNull <L2, R2> Either<L2, R2> map(Function<L, L2> left, Function<R, R2> right) {
    return Either.right(right.apply(value));
  }

  @Override
  public boolean isLeft() {
    return false;
  }

  @Override
  public boolean isRight() {
    return true;
  }

  @Override
  public L fromLeft(@Nullable L defaultValue) {
    return defaultValue;
  }

  @Override
  public R fromRight(@Nullable R defaultValue) {
    return this.value;
  }

  @Override
  public L leftOr(Function<? super R, ? extends L> mapper) {
    return mapper.apply(value);
  }

  @Override
  public R rightOr(Function<? super L, ? extends R> mapper) {
    return this.value;
  }

  @Override
  public String toString() {
    return "Either.Right(" + this.value + ")";
  }
}
