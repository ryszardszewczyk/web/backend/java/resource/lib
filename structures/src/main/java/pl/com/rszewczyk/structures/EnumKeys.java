package pl.com.rszewczyk.structures;

import java.util.List;

public interface EnumKeys {
  List<String> keys();
}
