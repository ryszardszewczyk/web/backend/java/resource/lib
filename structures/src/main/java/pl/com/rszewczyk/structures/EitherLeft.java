package pl.com.rszewczyk.structures;

import java.util.function.Consumer;
import java.util.function.Function;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EitherLeft<L, R> implements Either<L, R> {
  private final L value;

  public <X> X reduce(
      Function<? super L, ? extends X> left, Function<? super R, ? extends X> right) {
    return left.apply(value);
  }

  @Override
  public void Consume(Consumer<? super L> left, Consumer<? super R> right) {
    left.accept(value);
  }

  @Override
  public @NotNull <L2, R2> Either<L2, R2> map(Function<L, L2> left, Function<R, R2> right) {
    return Either.left(left.apply(value));
  }

  @Override
  public boolean isLeft() {
    return true;
  }

  @Override
  public boolean isRight() {
    return false;
  }

  @Override
  public L fromLeft(@Nullable L defaultValue) {
    return this.value;
  }

  @Override
  public R fromRight(@Nullable R defaultValue) {
    return defaultValue;
  }

  @Override
  public L leftOr(Function<? super R, ? extends L> mapper) {
    return this.value;
  }

  @Override
  public R rightOr(Function<? super L, ? extends R> mapper) {
    return mapper.apply(this.value);
  }

  @Override
  public String toString() {
    return "Either.Left(" + this.value + ")";
  }
}
