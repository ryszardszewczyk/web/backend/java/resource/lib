package pl.com.rszewczyk.structures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import javax.validation.constraints.NotNull;

public interface Pair<A, B> extends Single<A> {
  @NotNull
  B second();

  static <A, B> Pair<A, B> of(@NotNull A first, @NotNull B second) {
    return new Tuple2<>(first, second);
  }

  static <A, B> Pair<List<A>, List<B>> partition(@NotNull Collection<Pair<A, B>> pairs) {
    List<A> firsts = new ArrayList<>(pairs.size());
    List<B> seconds = new ArrayList<>(pairs.size());

    pairs.forEach(
        pair -> {
          firsts.add(pair.first());
          seconds.add(pair.second());
        });

    return of(firsts, seconds);
  }

  default <R1, R2> Pair<R1, R2> map(
      @NotNull Function<A, R1> firstMapper, @NotNull Function<B, R2> secondMapper) {
    return of(firstMapper.apply(this.first()), secondMapper.apply(this.second()));
  }
}
