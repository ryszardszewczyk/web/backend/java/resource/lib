package pl.com.rszewczyk.structures;

import java.util.List;
import java.util.function.Supplier;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Tuple2<A, B> implements Pair<A, B> {
  private final A first;
  private final B second;

  @Override
  public A first() {
    return this.first;
  }

  @Override
  public B second() {
    return this.second;
  }

  @Override
  public String toString() {
    return String.format("(%s, %s)", this.first, this.second);
  }

  @Override
  public List<Supplier<Object>> methods() {
    return List.of(this::first, this::second);
  }
}
