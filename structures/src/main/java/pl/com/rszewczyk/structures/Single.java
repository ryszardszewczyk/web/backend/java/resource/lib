package pl.com.rszewczyk.structures;

import javax.validation.constraints.NotNull;

public interface Single<A> extends Tuple {
  @NotNull
  A first();

  static <A> Single<A> of(@NotNull A first) {
    return new Tuple1<>(first);
  }
}
