package pl.com.rszewczyk.structures;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import lombok.RequiredArgsConstructor;
import pl.com.rszewczyk.lib.interfaces.MetadataSource;

@RequiredArgsConstructor
public class FileMetadataSource implements MetadataSource {
  private final File file;
  private final String contentType;

  @Override
  public InputStream getInputStream() throws IOException {
    return Files.newInputStream(this.file.toPath());
  }

  @Override
  public String getContentType() {
    return this.contentType;
  }

  @Override
  public String getName() {
    return this.file.getName();
  }

  @Override
  public String toString() {
    return "FileMetadataSource{" + "file=" + file + ", contentType='" + contentType + '\'' + '}';
  }
}
