package pl.com.rszewczyk.structures;

import java.util.List;
import java.util.function.Supplier;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Tuple1<A> implements Single<A> {
  private final A first;

  @Override
  public A first() {
    return this.first;
  }

  @Override
  public String toString() {
    return String.format("(%s)", this.first);
  }

  @Override
  public List<Supplier<Object>> methods() {
    return List.of(this::first);
  }
}
