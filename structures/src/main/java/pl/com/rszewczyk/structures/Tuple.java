package pl.com.rszewczyk.structures;

import java.io.Serializable;
import java.util.List;
import java.util.function.Supplier;

public interface Tuple extends Serializable {
  List<Supplier<Object>> methods();
}
